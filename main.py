from grammatics import Grammatics

SEQUNCE_COUNT = 5

def main():
    grammatics = Grammatics(["caaab", "bbaab", "caab", "bbab", "cab", "bbb", "cb"])
    print("Grammatics: ", grammatics.rules)
    print("Sequences: ", [grammatics.generate() for _ in range(SEQUNCE_COUNT)])

main()