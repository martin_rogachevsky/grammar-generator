from collections import defaultdict, Counter
from random import randint

class Grammatics:
    def __init__(self, sequences):
        self.initTerminals(sequences)
        self.initRules(sequences)
    
    def initTerminals(self, sequences):
        self.terminals = set()
        for seq in sequences:
            for symbol in seq:
                if not symbol in self.terminals:
                    self.terminals.add(symbol)
  
    def part1(self, sequences):
        self.rules = defaultdict(list)
        sortSeq = sorted(sequences)
        maxLen = len(sortSeq[0])
        maxLenSeq = {seq for seq in sortSeq if len(seq) == maxLen}
        index = 1
        for seq in sorted(maxLenSeq):
            l = len(seq)
            nonTerminal = 0
            while l > 2:
                if nonTerminal > 0:
                    self.rules[nonTerminal].append([seq[-l], index + 1])
                    index += 1
                else:
                    self.rules[nonTerminal].append([seq[-l], index])
                nonTerminal = index
                l -= 1
            self.rules[index].append([seq[-l], seq[-l + 1]])
            index += 1
        for seq in [seq for seq in sequences if not seq in maxLenSeq]:
            nonTerminal = 0
            suited = True
            for sym in seq:
                suitedRule = None
                if suited:
                    for rule in self.rules[nonTerminal]:
                        if rule[0] == sym:
                            suitedRule = rule
                            break
                if suitedRule == None:
                    suited = False
                    if sym != seq[-1]:
                        self.rules[nonTerminal].append([sym, index])
                        nonTerminal = index
                        index += 1
                    else:
                        self.rules[nonTerminal].append([sym])
                else:
                    nonTerminal = suitedRule[1]

    def part2(self):
        remainin = []
        for key in self.rules.keys():
            for rule in self.rules[key]:
                if len(rule) == 2 and rule[0] in self.terminals and rule[1] in self.terminals:
                    remainin.append((key, rule))
        for re in remainin:
            for key in self.rules.keys():
                if key != re[0]:
                    subs = [rule for rule in self.rules[key] if len(rule) == 2 and rule[0] == re[1][0] and not rule[1] in self.terminals]
                    if (len(subs)):
                        ends = [rule for rule in self.rules[subs[0][1]] if len(rule) == 1 and rule[0] == re[1][1]]
                        if len(ends) > 0:
                            for rule in self.rules[re[0]]:
                                if len([r for r in self.rules[key] if Counter(rule) == Counter(r)]) == 0 and Counter(rule) != Counter(re[1]):
                                    self.rules[key].append(rule)
                            for k in self.rules.keys():
                                for r in self.rules[k]:
                                    for ind, s in enumerate(r):
                                        if s == re[0]:
                                            r[ind] = key
                            self.rules.pop(re[0])
                            break    

    def part3(self):
        same = True
        while same:
            same = False
            for key1 in self.rules.keys():
                for key2 in self.rules.keys():
                    if key1 != key2 and len(self.rules[key1]) == len(self.rules[key2]):
                        same = True
                        for sym in self.rules[key1]:
                            if len([s for s in self.rules[key2] if Counter(s) == Counter(sym)]) == 0:
                                same = False
                                break
                        if same:
                            for k in self.rules.keys():
                                for r in self.rules[k]:
                                    for ind, s in enumerate(r):
                                        if s == key2:
                                            r[ind] = key1
                            self.rules.pop(key2)
                            break
                if same:
                    break


    def initRules(self, sequences):
        self.part1(sequences)
        self.part2()
        self.part3()

    def generate(self):
        cur = 0
        res = ''
        while True:
            choice = randint(0, len(self.rules[cur]) - 1)
            res += self.rules[cur][choice][0]
            if len(self.rules[cur][choice]) == 1 or self.rules[cur][choice][1] in self.terminals:
                break
            else:
                cur = self.rules[cur][choice][1]
        return res

            

